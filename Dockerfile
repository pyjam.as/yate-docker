FROM python:3.8
RUN apt-get update
RUN apt-get install subversion
RUN apt-get install make
RUN apt-get install autoconf
RUN apt-get install c++
RUN svn checkout http://voip.null.ro/svn/yate/trunk yate-SVN
WORKDIR yate-SVN
RUN ./autogen.sh
RUN ./configure
RUN make
CMD ./run
